<!-- $theme: gaia -->
<!-- $size: 16:9 -->

![](https://connpass-tokyo.s3.amazonaws.com/thumbs/28/0e/280e086df3279ad2614b116b7f344453.png)

# Git入門 with GitLab

@tnir (Takuya Noguchi)
[GitLab Women (GitLab Meetup Tokyo #11)](https://gitlab-jp.connpass.com/event/105178/)
2018-11-07 Creation Line, Inc. Akihabara

---

## Takuya Noguchi

- [@tnir](https://github.com/tnir) / Tw: @tn961ir / FB: takuya.noguchi.961
- Git (2005-), GitHub (2008-)
- GitLab: assessment (2012), user (2013-), admin (2014-), dev (2015-), [core](https://about.gitlab.com/community/core-team/) (2017-)
- Python, Go programmer, OSS contributor
- BizDev thru Dev+DevOps
- Docker, Cloud Native

---

# GitLab

---

# Git

---

# Git

Git is the center of software development.
Git is the center of software systems.

---

# Chemistry

> Chemistry is often called "The central science"

[The central science from Wikipedia](https://en.wikipedia.org/wiki/The_central_science)

---

![](https://upload.wikimedia.org/wikipedia/commons/0/04/Partial_ordering_of_the_sciences_Balaban_Klein_Scientometrics2006_615-637.svg)

---

# 似てる！

---

# Git

- ソフトウェア本体
- インフラストラクチャ (infrastructure as code)

---


# オススメの本

湊川 あい「わかばちゃんと学ぶ Git使い方入門」

---

# GitHub

---

# GitHub.com

- 2008年リリース（サービス）
- Bitbucketを押さえてデファクトスタンダードに
- OSSの開発場所としても活用
- Marketplaceで機能拡張できてよい :smile:

---

# GitLab

---

# GitLab

- 2011年リリースの**OSS**
- 2012年SaaS版 (.com) も出た
- どんな感じか触るには.comがオススメ
  - https://gitlab.com/
- **無制限private repository** with CI/CD
- フルスタックDevOpsソリューション
- Version Control System (Git), Issues, CI/CD, Registry & Artifacts, Monitoring, Environements etc.

---

# Lifecycle

![](https://about.gitlab.com/direction/product-vision/devops-loop-and-spans.png)

See [Stages of DevOps lifecyle](https://about.gitlab.com/stages-devops-lifecycle/)


---

# Issues (Plan)

---

# ![](https://image.slidesharecdn.com/gitlab-issue-tracker-180410111749/95/gitlab-1-638.jpg)

---

# Issueドリブンプロジェクト

- issueを明確にしてから作業する
- e.g.) 家事, プロジェクト
- GitLab初心者は家のタスク管理をGitLab Issuesで始めたらいいのでは？

- cf. GitLab Meetup Tokyo #7 by @jumpyoshim
  - Qiita: https://qiita.com/jumpyoshim/items/f0ee99d770192c48fc7e
  - スライド: https://www.slideshare.net/JumpeiYoshimura/gitlab-93438104
- cf. [Backlogを利用した家庭内のタスク管理について報告してみる](http://www.afroriansym100life-shift.net/entry/2018/01/14/110312)

---

# Kanban / Issue board

- アジャイル・スクラムに最適なツール
- GitHub Projectsと同じもしくはそれ以上に使える
- ソースコードと密に連携→アジリティ向上

---

# Workflow (Create)

---

# Workflow

- git-flow
  - あまりオススメできない
- GitHub Flow
  - ちゃんと運用するのはやや難しい
- GitLab Flow
  - git-flowからの移行もオススメ

---

# GitLab Flow

https://about.gitlab.com/2014/09/29/gitlab-flow/

![](https://about.gitlab.com/images/git_flow/release_branches.png)

---

# チーム開発にも優しいGitLab

- `master` ブランチのprotectがデフォルトで設定されている
  - ついつい忘れてしまってプロジェクトが始まることがない
- 外部CI/CD設定も不要 (Verify/Package/Release)
  - 見よう見まねでCI YAMLをコピってくるだけでだいたい動く
  - （要議論）

---

# GitLabとCloud Native

---

# Cloud Native

- コードを書いてすぐにデプロイすること
- GitLabでは2015年頃から取り組み→2015年OpenShift連携リリース
- →2016年5月、Kubernetes連携リリース

---

# Container Registry

- コンテナを使うにはコンテナイメージの管理が必要不可欠
- GitLabにはGitLab Container Registry
  - 小〜中規模であればそのまま使える
  - 大規模でも少しadminすれば使える
  - 権限設定

---

# Configure/Monitor/Secure

- OSS版(Core, Community)では最低限のカバー
- Secureまで含めるとEnterprise版 or 別ソリューション (Google Cloud etc.) を検討したほうがよいかもしれない


---

# GitLab Pages

---

# GitLab Pages

- 高機能版GitHub Pages
  - 外部CI/CD連携不要
- Markdownを書くだけでドキュメント共有
- 閲覧権限管理は11月リリースのv11.5でリリース見込み

---

# All features

- https://about.gitlab.com/features/ を見てください

---

# Community

---

# Community

- GitLab Tokyo
- GitLab . JP

→ https://www.meetup.com/GitLab-Meetup-Tokyo/

---

# コミュニティはオフラインだけではない

---

# コミュニティ

- JP Slack
- Gitter ([gitter.im/gitlabhq/gitlabhq](https://gitter.im/gitlabhq/gitlabhq))
- GitLab翻訳 (英→日)
- Docs (英語)
- コードコントリビューション
  - Ruby on Rails, Go, Vue.js, CSS

---

# GitLab Summit

- 9ヶ月に1度のGitLabイベント
- 前回は2018年8月南アフリカケープタウン
- 次回は2019年5月[ニューオリンズ](https://gitlab.com/gitlabcontribute/new-orleans)

https://about.gitlab.com/company/culture/summits/

---

# 11/20 Women Who Code Tokyo

https://www.meetup.com/Women-Who-Code-Tokyo/events/255736067/

![](./womenwhocodetokyo.jpeg)

