# Git入門 with GitLab

@tnir (Takuya Noguchi)
2018-11-07 Creation Line, Inc. Akihabara

- 2018-11-07
- [GitLab Women (GitLab Meetup Tokyo #11)](https://gitlab-jp.connpass.com/event/105178/), Creation Line, Inc. Akihabara
- hash tag: #gitlabjp


## Copyright

2018 Takuya Noguchi <takninnovationresearch@gmail.com>
